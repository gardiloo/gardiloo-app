import gql from "graphql-tag";

export const DELETE_POST = gql`
    mutation deletePost ($data: DeletePostInput!) {
        deletePost(data: $data) {
            title
        }
    }
`;
