import gql from "graphql-tag";

export const LOGIN = gql`
    mutation login ($data: LoginUserInput!) {
        login(data: $data){
            token
        }
    }
`;
