import { CREATE_DRAFT } from "./createDraft";
import { CREATE_USER } from "./createUser";
import { LOGIN } from "./login";
import { DELETE_POST } from "./deletePost";

const gardilooMutations = {
    CREATE_DRAFT,
    CREATE_USER,
    LOGIN,
    DELETE_POST,
};

export default gardilooMutations;
