import gql from "graphql-tag";

export const CREATE_DRAFT = gql`    
    mutation createDraft ($data: CreateDraftInput!) {
        createDraft(data: $data){
            title
            price
        }
    }
`;
