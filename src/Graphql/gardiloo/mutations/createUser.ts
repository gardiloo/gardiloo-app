import gql from "graphql-tag";

export const CREATE_USER = gql`
    mutation createUser ($data: CreateUserInput!) {
        createUser(data: $data){
            token
        }
    }
`;
