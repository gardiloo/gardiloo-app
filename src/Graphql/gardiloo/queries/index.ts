import { FEED } from "./feed";
import { FILTER_POSTS } from "./filterPosts";
import { ME } from "./me";
import { USER_POSTS } from "./userPosts";

const gardilooQueries = {
    FEED,
    FILTER_POSTS,
    ME,
    USER_POSTS
};

export default gardilooQueries;
