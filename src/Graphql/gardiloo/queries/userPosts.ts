
import gql from "graphql-tag";

export const USER_POSTS = gql`
    query userPosts {
        userPosts {
            id
            price
            title
            isbn
            createdAt
        }
    }
`;
