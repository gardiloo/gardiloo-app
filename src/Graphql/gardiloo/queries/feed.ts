import gql from "graphql-tag";

export const FEED = gql`
    query feed {
        feed {
            id
            price
            title
            content
            isbn
            image
            createdAt
            author {
                email
            }
        }
    }
`;
