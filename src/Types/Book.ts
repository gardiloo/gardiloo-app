export default interface Book {
    authors: string[];
    title: string;
    subtitle: string;
    description: string;
    image: string | undefined;
}
