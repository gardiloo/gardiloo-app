import React from 'react';
import MainToolbar from '../Components/MainToolbar/MainToolbar';
import BookCardListing from '../Components/BookListing/BookCardListing';
import AuthenticateModal from '../Components/AuthenticateModal/AuthenticateModal';
import MyPosts from '../Components/MyPosts/MyPosts';
import MyPostsListing from '../Components/MyPostsListing/MyPostListing';

const Main: React.FC = () => {

    const [openLoginModal, setOpenLoginModal] = React.useState<boolean>(false);
    const [openSignUpModal, setOpenSignUpModal] = React.useState<boolean>(false);
    const [openPostModal, setOpenPostModal] = React.useState<boolean>(false);
    const [bufferSearch, setBufferSearch] = React.useState<string>("");
    const [mobileOpen, setMobileOpen] = React.useState(false);


    const token = localStorage.getItem('token');

    return (
        <>
            <MainToolbar
                setOpenLoginModal={setOpenLoginModal}
                setOpenSignUpModal={setOpenSignUpModal}
                setOpenPostModal={setOpenPostModal}
                setBufferSearchString={setBufferSearch}
                mobileOpen={mobileOpen}
                setMobileOpen={setMobileOpen}
            />
            <BookCardListing
                bufferSearch={bufferSearch}
                openPostModal={openPostModal}
                setOpenPostModal={setOpenPostModal}
            />
            <AuthenticateModal
                openLoginModal={openLoginModal}
                openSignUpModal={openSignUpModal}
                setOpenLoginModal={setOpenLoginModal}
                setOpenSignUpModal={setOpenSignUpModal}
            />
            {
                token &&
                <MyPostsListing
                    mobileOpen={mobileOpen}
                    setMobileOpen={setMobileOpen}
                >
                    <MyPosts />
                </MyPostsListing>
            }
        </>
    );
};

export default Main;
