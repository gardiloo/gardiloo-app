import React from 'react';
import {hot} from 'react-hot-loader';
import {ApolloProvider} from 'react-apollo';
import ApolloClient from 'apollo-boost';
import config from '../config';
import MainLayout from './Layout/Main';
import { SnackbarProvider } from 'notistack';
import ConfirmationModalProvider from './Components/ConfirmationModal/ConfirmationModalProvider';

const App = () => {

    const token = localStorage.getItem('token');

    const client = new ApolloClient({
        uri: config.endpoints.gardiloo_api,
        credentials: 'include',
        headers: {
            authorization: token ? `Bearer ${token}` : "",
        }
    });

    return (
        <ApolloProvider client={client}>
            <SnackbarProvider>
                <ConfirmationModalProvider>
                   <MainLayout />
                </ConfirmationModalProvider>
            </SnackbarProvider>
        </ApolloProvider>
    );
};

export default (process.env.NODE_ENV === 'development' ? hot(module)(App) : App);
