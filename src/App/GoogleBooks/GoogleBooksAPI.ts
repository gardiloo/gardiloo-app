import BookAPI from '../../Types/BookAPI';

export default interface GoogleBooksAPI extends BookAPI {
    getKey(): string | undefined;
    hasKey(): boolean;
}
