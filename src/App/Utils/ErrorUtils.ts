import { ApolloError } from 'apollo-client';

export const formatGraphQLError = (error: ApolloError): string => {
    const regex = /^(GraphQL error: )(.*)$/;

    const matches = error.message.match(regex);

    if (matches) {
        return matches[2];
    }

    return error.message;
};
