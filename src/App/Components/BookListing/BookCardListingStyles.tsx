import {makeStyles} from '@material-ui/core';

export const useBookCardListingStyles = makeStyles(theme => ({
    bookContainer: {
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "center"
    },
    progressBar: {
        [theme.breakpoints.down('sm')]: {
            position: 'relative',
            top: "-4px"
        },
        position: 'relative',
        top: '4px'
    },
    listingContainer: {
        backgroundColor: "rgb(220, 241, 247)",
        paddingTop: "60px",
        boxShadow: "none"
    },
}));
