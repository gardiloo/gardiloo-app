import React from 'react';
import BookCard from '../BookCard/BookCard';
import {useBookCardListingStyles} from './BookCardListingStyles';
import Post, {PostAsObject} from '../../../Models/Post';
import {useQuery} from '@apollo/react-hooks';
import gardilooQueries from '../../../Graphql/gardiloo/queries';
import PostBookModal from '../PostBookModal/PostBookModal';
import {LinearProgress, Paper} from '@material-ui/core';

interface BookCardListingProps {
    bufferSearch: string;
    openPostModal: boolean;
    setOpenPostModal: (open: boolean) => void;
}

const BookCardListing: React.FC<BookCardListingProps> = (props: BookCardListingProps) => {

    const {bufferSearch, openPostModal, setOpenPostModal} = props;

    const classes = useBookCardListingStyles();

    const {loading, error, data, refetch} = useQuery(gardilooQueries.FILTER_POSTS, {
        variables: {searchString: bufferSearch}
    });

    const { data: me } = useQuery(gardilooQueries.ME);

    if (error) {
        return <div>error...</div>;
    }

    if (loading) {
        return <LinearProgress className={classes.progressBar}/>;
    }

    const posts: Post[] = data.filterPosts.map((post: PostAsObject) => Post.createFromJSONObject(post));

    const loggedInUser = !!me ? me.me.email : null;

    return (
        <Paper className={classes.listingContainer}>
            <div className={classes.bookContainer}>
                {
                    posts.map((post, index) => {
                        let isEditable = post.author.email !== null && post.author.email === loggedInUser;

                        return (
                            <BookCard
                                key={index}
                                id={post.id}
                                price={post.price}
                                title={post.title}
                                image={post.image}
                                createdAt={post.createdAt}
                                isEditable={isEditable}
                            />
                        );
                    })
                }
                <PostBookModal
                    open={openPostModal}
                    setOpen={setOpenPostModal}
                    refetchPosts={refetch}
                />
            </div>
        </Paper>
    );
};

export default BookCardListing;

