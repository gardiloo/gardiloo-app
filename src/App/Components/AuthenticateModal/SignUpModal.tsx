import React, {useState} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {useMutation} from '@apollo/react-hooks';
import gardilooMutations from '../../../Graphql/gardiloo/mutations';
import { ApolloError } from 'apollo-boost';
import { useSnackbar } from 'notistack';
import { formatGraphQLError } from '../../Utils/ErrorUtils';

interface SignUpModalProps {
    email: string;
    password: string;
    open: boolean;
    switchModals: () => void;
    setOpen: (open: boolean) => void;
    setEmail: (email: string) => void;
    setPassword: (password: string) => void;
}

const SignUpModal: React.FC<SignUpModalProps> = (props: SignUpModalProps) => {

    const {open, setOpen, email, password, setEmail, setPassword, switchModals} = props;

    const [name, setName] = useState<string>();

    const [signupMutation] = useMutation(gardilooMutations.CREATE_USER);

    const {enqueueSnackbar} = useSnackbar();

    const handleClose = () => setOpen(false);

    const handleSignup = async () => {
        signupMutation({
            variables: {
                data: {
                    email: email,
                    name: name,
                    password: password
                }
            }
        }).then(data => {
            localStorage.setItem('token', data.data.createUser.token);
            setOpen(false);
            window.location.reload();
        }).catch((error: ApolloError) => {
            enqueueSnackbar(formatGraphQLError(error), {variant: 'error'});
        });
    };

    return (
        <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="form-dialog-title"
        >
            <DialogTitle id="form-dialog-title">{"Sign up"}</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    To post your books, please {"sign up"}
                </DialogContentText>
                <TextField
                    autoFocus
                    margin="dense"
                    id="name"
                    label="Name"
                    type="text"
                    onChange={e => setName(e.target.value)}
                    fullWidth
                />
                <TextField
                    margin="dense"
                    id="email"
                    label="Email"
                    type="email"
                    onChange={e => setEmail(e.target.value)}
                    fullWidth
                />
                <TextField
                    margin="dense"
                    id="password"
                    label="Password"
                    type="password"
                    onChange={e => setPassword(e.target.value)}
                    fullWidth
                />
                <DialogContentText>
                    Already have an account?
                    <Button onClick={switchModals}>Login</Button>
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="primary">
                    Cancel
                </Button>
                <Button onClick={handleSignup} color="primary">
                    {"Sign up"}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default SignUpModal;
