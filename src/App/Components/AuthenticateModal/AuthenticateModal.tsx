import React, {useEffect, useState} from 'react';
import LoginModal from './LoginModal';
import SignUpModal from './SignUpModal';

interface AuthenticateModalProps {
    openLoginModal: boolean;
    openSignUpModal: boolean;
    setOpenLoginModal: (open: boolean) => void;
    setOpenSignUpModal: (open: boolean) => void;
}

const AuthenticateModal: React.FC<AuthenticateModalProps> = (props: AuthenticateModalProps) => {

    const {setOpenLoginModal, setOpenSignUpModal, openLoginModal, openSignUpModal} = props;

    const [email, setEmail] = useState<string>("");
    const [password, setPassword] = useState<string>("");

    useEffect(() => setPassword(""), [openSignUpModal, openLoginModal]);

    const switchModals = () => {
        setOpenLoginModal(!openLoginModal);
        setOpenSignUpModal(openLoginModal);
    };

    return (
        <>
            <LoginModal
                setEmail={setEmail}
                setPassword={setPassword}
                setOpen={setOpenLoginModal}
                open={openLoginModal}
                email={email}
                password={password}
                switchModals={switchModals}
            />
            <SignUpModal
                setEmail={setEmail}
                setPassword={setPassword}
                setOpen={setOpenSignUpModal}
                open={openSignUpModal}
                email={email}
                password={password}
                switchModals={switchModals}
            />
        </>
    );
};

export default AuthenticateModal;
