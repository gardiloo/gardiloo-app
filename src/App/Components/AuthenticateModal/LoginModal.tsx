import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {useMutation} from '@apollo/react-hooks';
import gardilooMutations from '../../../Graphql/gardiloo/mutations';
import { ApolloError } from 'apollo-boost';
import { useSnackbar } from 'notistack';
import { formatGraphQLError } from '../../Utils/ErrorUtils';

interface LoginModalProps {
    email: string;
    password: string;
    open: boolean;
    switchModals: () => void;
    setOpen: (open: boolean) => void;
    setEmail: (email: string) => void;
    setPassword: (password: string) => void;
}

const LoginModal: React.FC<LoginModalProps> = (props: LoginModalProps) => {

    const {open, setOpen, setEmail, setPassword, email, password, switchModals} = props;

    const [loginMutation] = useMutation(gardilooMutations.LOGIN);

    const {enqueueSnackbar} = useSnackbar();

    const handleClose = () => setOpen(false);

    const handleLogin = async () => {
        loginMutation({
            variables: {
                data: {
                    email: email,
                    password: password
                }
            }
        }).then(data => {
            localStorage.setItem('token', data.data.login.token);
            setOpen(false);
            window.location.reload();
        }).catch((error: ApolloError) => {
            enqueueSnackbar(formatGraphQLError(error), {variant: 'error'});
        });
    };

    return (
        <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="form-dialog-title"
        >
            <DialogTitle id="form-dialog-title">{"Log in"}</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    To post your books, please {"log in"}
                </DialogContentText>
                <TextField
                    autoFocus
                    margin="dense"
                    id="email"
                    label="Email"
                    type="email"
                    onChange={e => setEmail(e.target.value)}
                    fullWidth
                />
                <TextField
                    margin="dense"
                    id="password"
                    label="Password"
                    type="password"
                    onChange={e => setPassword(e.target.value)}
                    fullWidth
                />
                <DialogContentText>
                    Don't have an account?
                    <Button onClick={switchModals}>Sign up</Button>
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="primary">
                    {"Cancel"}
                </Button>
                <Button onClick={handleLogin} color="primary">
                    {"Login"}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default LoginModal;
