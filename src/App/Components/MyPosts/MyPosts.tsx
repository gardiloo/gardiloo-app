import React, {useContext} from 'react';
import {useMyPostsStyles} from './MyPostsStyles';
import Post, {PostAsObject} from '../../../Models/Post';
import {useQuery} from '@apollo/react-hooks';
import gardilooQueries from '../../../Graphql/gardiloo/queries';
import {CardHeader, IconButton, LinearProgress, Card, Divider, Typography} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import {ConfirmationModalContext} from '../ConfirmationModal/ConfirmationModalProvider';

const dateFormatOptions = {day: 'numeric', month: 'short', year: 'numeric'};

const MyPosts: React.FC = () => {

    const classes = useMyPostsStyles();

    const {loading, error, data} = useQuery(gardilooQueries.USER_POSTS);

    const confirmModalContext = useContext(ConfirmationModalContext);

    if (error) {
        return <div>error...</div>;
    }

    if (loading) {
        return <LinearProgress style={{position: 'relative', top: '4px'}}/>;
    }

    const posts: Post[] = data.userPosts.map((post: PostAsObject) => Post.createFromJSONObject(post));

    const handleDelete = async (postId: any, title: string) => {

        if (!!confirmModalContext) {
            const {setOpen, setAction, setPostId, setQuery} = confirmModalContext;

            setPostId(postId);
            setAction(`${title}`);
            setOpen(true);
            setQuery(gardilooQueries.USER_POSTS);
        }

    };

    return (
        <div className={classes.postContainer}>
            <Typography style={{padding: "10px", width: "115px", margin: "0px auto"}}>
                My Posts
            </Typography>
            <Divider/>
            {
                posts.map((post, index) => {

                    const date: string = new Date(post.createdAt).toLocaleDateString('default', dateFormatOptions);

                    return (
                        <Card
                            key={index}
                            className={classes.card}
                        >
                            <CardHeader

                                className={classes.myPosts}
                                action={
                                    <IconButton
                                        onClick={() => handleDelete(post.id, post.title)}
                                        aria-label="settings">
                                        <CloseIcon/>
                                    </IconButton>
                                }
                                classes={{
                                    subheader: classes.subHeader,
                                    title: classes.title
                                }}
                                title={`${post.price} ${post.title}`}
                                subheader={`${date} ISBN:${post.isbn}`}
                            />
                        </Card>
                    )
                })
            }
        </div>
    );
};

export default MyPosts;

