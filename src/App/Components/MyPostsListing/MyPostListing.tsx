import React from 'react';
import {useMyPostsStyles} from './MyPostsListingStyles';
import {Hidden, Drawer} from '@material-ui/core';

interface MyPostsListingProps {
    children: any;
    setMobileOpen: (open: boolean) => void;
    mobileOpen: boolean;
}

const MyPostsListing: React.FC<MyPostsListingProps> = (props: MyPostsListingProps) => {

    const {children, mobileOpen, setMobileOpen} = props;

    const classes = useMyPostsStyles();

    const handleDrawerToggle = () => setMobileOpen(!mobileOpen);

    return (
        <>
            <Hidden smUp implementation="css">
                <Drawer
                    variant="temporary"
                    anchor={'right'}
                    open={mobileOpen}
                    onClose={handleDrawerToggle}
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                    ModalProps={{
                        keepMounted: true, // Better open performance on mobile.
                    }}
                >
                    {children}
                </Drawer>
            </Hidden>
        </>
    );
};

export default MyPostsListing;

