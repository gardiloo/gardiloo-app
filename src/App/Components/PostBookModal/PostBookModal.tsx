import React, {useState} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import {useMutation} from '@apollo/react-hooks';
import gardilooMutations from '../../../Graphql/gardiloo/mutations';
import GoogleBooks from '../../GoogleBooks/GoogleBooks';
import Book from '../../../Types/Book';
import config from '../../../config';
import {formatGraphQLError} from '../../Utils/ErrorUtils';
import {useSnackbar} from 'notistack';
import {TextareaAutosize} from '@material-ui/core';

interface PostBookModalProps {
    open: boolean;
    setOpen: (open: boolean) => void;
    refetchPosts: () => void;
}

const PostBookModal: React.FC<PostBookModalProps> = (props: PostBookModalProps) => {

    const {open, setOpen, refetchPosts} = props;

    const [isbn, setIsbn] = useState<string>();
    const [price, setPrice] = useState<string>();
    const [content, setContent] = useState<string>();
    const [posting, setPosting] = useState<boolean>(false);

    const [createDraft] = useMutation(gardilooMutations.CREATE_DRAFT);

    const {enqueueSnackbar} = useSnackbar();

    const googleBooks = new GoogleBooks(config.api_keys.google_books_api_key);

    const handleClose = () => setOpen(false);

    const handlePost = async () => {
        googleBooks.search(`isbn:${isbn}`).then(async (books: Book[]) => {
            setPosting(true);
            createDraft({
                variables: {
                    data: {
                        isbn: isbn,
                        price: price,
                        content: content,
                        title: books[0].title,
                        image: books[0].image
                    }
                }
            }).then(result => {
                const title = result.data.createDraft.title;

                enqueueSnackbar(`Posted ${title}`, {variant: 'success'});
                setOpen(false);
                refetchPosts();

            }).catch(error => {
                enqueueSnackbar(formatGraphQLError(error), {variant: 'error'});
            });
        }).catch(error => {
            enqueueSnackbar(error.message, {variant: 'error'});
        });

    };

    return (
        <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="form-dialog-title"
        >
            <DialogTitle id="form-dialog-title">Post your book</DialogTitle>
            <DialogContent>
                <TextField
                    autoFocus
                    margin="dense"
                    id="isbn"
                    label="isbn"
                    type="text"
                    onChange={e => setIsbn(e.target.value)}
                    fullWidth
                    required
                />
                <TextField
                    margin="dense"
                    id="price"
                    label="price"
                    type="number"
                    onChange={e => setPrice(e.target.value)}
                    fullWidth
                    required
                />
                <TextareaAutosize
                    id="content"
                    defaultValue={"Enter a short description"}
                    onChange={e => setContent(e.target.value)}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="primary">
                    Cancel
                </Button>
                <Button disabled={posting} onClick={handlePost} color="primary">
                    Post
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default PostBookModal;
