import {makeStyles} from '@material-ui/core';

export const useConfirmationModalStyles = makeStyles(theme => ({
    postContainer: {
        [theme.breakpoints.down('sm')]: {
            top: 0
        },
        position: "absolute",
        top: "70px",
        background: "#f6f6f6"
    },
    card: {
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "center",
        margin: "8px",
    },
    title: {
        fontSize: "12px"
    },
    subHeader: {
        fontSize: "12px"
    },
    myPosts: {
        margin: "5px"
    }
}));
