import React from 'react';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import {useMutation, useQuery} from '@apollo/react-hooks';
import gardilooMutations from '../../../Graphql/gardiloo/mutations';
import {useSnackbar} from 'notistack';
import {DocumentNode} from 'graphql';
import gardilooQueries from '../../../Graphql/gardiloo/queries';

interface ConfirmationModalProps {
    postId: string;
    action: string
    query: DocumentNode | undefined;
    open: boolean;
    setOpen: any;
    queryVariables: any;
}

const ConfirmationModal: React.FC<ConfirmationModalProps> = (props: ConfirmationModalProps) => {

    const {action, open, setOpen, postId, query, queryVariables} = props;

    const [deletePost] = useMutation(gardilooMutations.DELETE_POST);

    const {refetch} = useQuery(query ? query : gardilooQueries.ME, queryVariables);

    const {enqueueSnackbar} = useSnackbar();

    const handleClose = () => setOpen(false);

    const handleConfirm = async () => {
        deletePost({variables: {data: {id: postId}}})
            .then(result => {
                enqueueSnackbar(`Successfully deleted ${result.data.deletePost.title}`, {variant: 'success'});
                setOpen(false);
                refetch();
            });
    };

    return (
        <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="form-dialog-title"
        >
            <DialogTitle id="form-dialog-title">Confirm</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Are you sure you want to delete <span style={{color: "#1C59D2"}}>{action}</span>
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="primary">
                    Cancel
                </Button>
                <Button onClick={handleConfirm} color="primary">
                    Confirm
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default ConfirmationModal;

