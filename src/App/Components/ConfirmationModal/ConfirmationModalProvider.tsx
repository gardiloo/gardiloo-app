import React, { useState } from "react";
import ConfirmationModal from './ConfirmationModal';
import {DocumentNode} from 'graphql';

interface ConfirmationModalContextInterface {
    setOpen: (open: boolean) => void;
    setAction: (action: string) => void;
    setPostId: (postId: string) => void;
    setQuery: (query: DocumentNode) => void;
    setQueryVariables: (setQueryVariables: any) => void;
}

export const ConfirmationModalContext = React.createContext<ConfirmationModalContextInterface | null>(null);

interface ConfirmationModalProviderProps {
    children: {}
}

const ConfirmationModalProvider: React.FC<ConfirmationModalProviderProps> = (props: ConfirmationModalProviderProps) => {
    const [open, setOpen] = useState<boolean>(false);
    const [action, setAction] = useState<string>("");
    const [postId, setPostId] = useState<string>("");
    const [query, setQuery] = useState<DocumentNode>();
    const [queryVariables, setQueryVariables] = useState<any>();

    return (
        <ConfirmationModalContext.Provider value={{
            setAction: setAction,
            setOpen: setOpen,
            setPostId: setPostId,
            setQuery: setQuery,
            setQueryVariables: setQueryVariables,
        }}>
            {props.children}
            <ConfirmationModal
                postId={postId}
                action={action}
                open={open}
                setOpen={setOpen}
                query={query}
                queryVariables={queryVariables}
            />
        </ConfirmationModalContext.Provider>
    );
};

export default ConfirmationModalProvider;
