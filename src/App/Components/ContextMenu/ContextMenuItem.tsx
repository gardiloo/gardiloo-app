import { Typography } from "@material-ui/core";
import React from "react";
import {useContextMenuStyles} from './ContextMenuStyles';

interface ContextMenuItemProps {
    clickHandler: () => void;
    iconText: string;
    icon: any;
}

const ContextMenuItem = ( props: ContextMenuItemProps ) => {
    const { clickHandler, iconText, icon } = props;

    const classes = useContextMenuStyles();

    return (
        <div onClick={clickHandler} className={classes.hoverSelect}>
            <div className={classes.menuEntry}>
                { icon }
                <Typography className={classes.iconText}>{iconText}</Typography>
            </div>
        </div>
    );
};

export default ContextMenuItem;
