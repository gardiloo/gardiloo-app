import React, {useRef, RefObject, useContext} from 'react';
import {Popover, Divider} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import ContextMenuItem from './ContextMenuItem';
import {useContextMenuStyles} from './ContextMenuStyles';
import {ConfirmationModalContext} from '../ConfirmationModal/ConfirmationModalProvider';
import gardilooQueries from '../../../Graphql/gardiloo/queries';

interface ContextMenuProps {
    open: boolean;
    postId: string;
    title: string;
    locationX: number;
    locationY: number;
    openContextMenu: (open: boolean) => void;
}

const ContextMenu: React.FC<ContextMenuProps> = (props: ContextMenuProps) => {
    const {open, locationX, locationY, openContextMenu, postId, title} = props;

    const node: RefObject<HTMLDivElement> = useRef<HTMLDivElement>(null);

    const classes = useContextMenuStyles();

    const confirmModalContext = useContext(ConfirmationModalContext);

    const handleDelete = async () => {

        if (!!confirmModalContext) {
            const {setOpen, setAction, setPostId, setQuery, setQueryVariables} = confirmModalContext;

            setPostId(postId);
            setAction(title);
            setOpen(true);
            setQuery(gardilooQueries.FILTER_POSTS);
            setQueryVariables({variables: {searchString: ""}});

            openContextMenu(false);
        }
    };

    return (
        <Popover
            open={open}
            onClose={() => openContextMenu(false)}
            anchorReference={'anchorPosition'}
            anchorPosition={{top: locationY, left: locationX}}
            anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
            }}
            transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
            }}
        >
            <div ref={node}>
                <Divider/>
                <div className={classes.hoverContainer}>
                    <ContextMenuItem
                        clickHandler={handleDelete}
                        iconText={'Remove'}
                        icon={<DeleteIcon className={classes.icon}/>}
                    />
                </div>
            </div>
        </Popover>
    );
};

export default ContextMenu;
