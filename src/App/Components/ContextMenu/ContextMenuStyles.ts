import {makeStyles} from '@material-ui/core';

export const useContextMenuStyles = makeStyles(() => ({
    menuEntry: {
        display: "flex",
        marginLeft: "20px",
        marginRight: "70px"
    },
    hoverContainer: {
        "&:hover": {
            cursor: "pointer"
        },
        marginTop: "10px",
        marginBottom: "10px"
    },
    hoverSelect: {
        "&:hover": {
            background: "#ededed",
        }
    },
    icon: {
        marginRight: "10px",
        color: "#5e6c82"
    },
    iconText: {
        paddingTop: "1.5px",
        justifyContent: "center"
    }
}));
