import {red} from '@material-ui/core/colors';
import {makeStyles} from '@material-ui/core';

export const useBookCardStyles = makeStyles(theme => ({
    card: {
        margin: '15px',
        height: 'auto',
        boxShadow: 'none',
        background: 'none'
    },
    cardHeader: {
        width: '125px',
        margin: '0px auto'
    },
    media: {
        height: 200,
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    avatar: {
        backgroundColor: red[500],
    },
    bookCardMedia: {
        backgroundSize: 'contain'
    },
    cardTitle: {
        fontSize: '12px'
    },
    cardSubheader: {
        fontSize: '12px'
    },

}));
