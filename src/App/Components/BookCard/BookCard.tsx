import React, {useState} from 'react';
import {useBookCardStyles} from './BookCardStyles';
import {Card, CardHeader, CardMedia, IconButton} from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import ContextMenu from '../ContextMenu/ContextMenu';

const dateFormatOptions = { day: "numeric", month: "short", year: "numeric" };

interface BookCardProps {
    id: string;
    price: string;
    title: string;
    isEditable: boolean;
    image: string;
    createdAt: string;
}

const BookCard: React.FC<BookCardProps> = (props: BookCardProps) => {

    const {price, title, isEditable, image, id, createdAt} = props;

    const [openContext, setOpenContext] = useState<boolean>(false);
    const [contextCoords, setContextCoords] = useState<number[]>([0,0]);

    const classes = useBookCardStyles();
    const date: string = new Date(createdAt).toLocaleDateString("default", dateFormatOptions);

    const handleContextMenuClick = (e: any) => {
        setContextCoords([e.clientX, e.clientY]);
        setOpenContext(true);
    };

    return (
        <>
            <Card className={classes.card}>
                <CardMedia
                    className={classes.media}
                    image={image}
                    classes={{root: classes.bookCardMedia}}
                    style={{backgroundSize: 'contain'}}
                    title="Paella dish"
                />
                <CardHeader
                    action={
                        isEditable &&
                        <IconButton
                            onClick={event => handleContextMenuClick(event)}
                            aria-label="settings">
                            <MoreVertIcon/>
                        </IconButton>
                    }
                    title={`$${price} - ${title}`}
                    subheader={date}
                    classes={{
                        title: classes.cardTitle,
                        subheader: classes.cardSubheader
                    }}
                    className={classes.cardHeader}
                />
            </Card>
            <ContextMenu
                open={openContext}
                postId={id}
                title={title}
                locationX={contextCoords[0]}
                locationY={contextCoords[1]}
                openContextMenu={setOpenContext}
            />
        </>
    );
};

export default BookCard;

