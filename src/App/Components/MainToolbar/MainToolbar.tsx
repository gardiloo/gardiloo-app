import React, {useEffect, useState} from 'react';
import {AppBar, InputBase, Typography, Toolbar, Button, IconButton} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import {useMainToolbarStyles} from './MainToolbarStyles';
import MenuIcon from '@material-ui/icons/Menu';
import * as _ from "lodash";

interface MainToolbarProps {
    setOpenPostModal: (open: boolean) => void;
    setOpenLoginModal: (open: boolean) => void;
    setOpenSignUpModal: (open: boolean) => void;
    setBufferSearchString: (searchString: string) => void;
    setMobileOpen: (open: boolean) => void;
    mobileOpen: boolean;
}

const MainToolbar: React.FC<MainToolbarProps> = (props: MainToolbarProps) => {

    const {setOpenPostModal, setOpenLoginModal, setOpenSignUpModal, setBufferSearchString, mobileOpen, setMobileOpen} = props;

    const [searchString, setSearchString] = useState<string>("");
    const [debouncedValue, setDebouncedValue] = useState<string>("");

    const classes = useMainToolbarStyles();

    const token = localStorage.getItem('token');

    const debounceSearch = _.debounce(s => setDebouncedValue(s), 2000);

    debounceSearch(searchString);

    useEffect(() => setBufferSearchString(debouncedValue), [debouncedValue]);

    const handleOpenPostModal = () => {
        if (token) {
            setOpenPostModal(true);
        } else {
            setOpenSignUpModal(true);
        }
    };

    const handleLogout = () => {
        localStorage.setItem('token', '');
        window.location.reload();
    };

    const handleDrawerToggle = () => setMobileOpen(!mobileOpen);

    const handleSearch = (event: any) => setSearchString(event.target.value);

    const handleOpenLoginModal = () => setOpenLoginModal(true);

    return (
        <div className={classes.grow}>
            <AppBar
                classes={{
                    colorPrimary: classes.appBarColor
                }}
                position="fixed"
            >
                <Toolbar>
                    <Typography className={classes.title} variant="h6" noWrap>
                        Gardiloo
                    </Typography>
                    <div className={classes.search}>
                        <div className={classes.searchIcon}>
                            <SearchIcon/>
                        </div>
                        <InputBase
                            placeholder="Search…"
                            onChange={handleSearch}
                            value={searchString}
                            autoFocus
                            classes={{
                                root: classes.inputRoot,
                                input: classes.inputInput,
                            }}
                            inputProps={{'aria-label': 'search'}}
                        />
                    </div>
                    <div className={classes.grow}/>
                    <div>
                        <Button
                            aria-label="show more"
                            aria-haspopup="true"
                            color="inherit"
                            onClick={handleOpenPostModal}
                        >
                            Post
                        </Button>

                    </div>

                    {
                        !token &&
                        <div>
                            <Button
                                aria-label="show more"
                                aria-haspopup="true"
                                color="inherit"
                                onClick={handleOpenLoginModal}
                            >
                                Login
                            </Button>
                        </div>
                    }
                    {
                        !!token &&
                        <div>
                            <Button
                                aria-label="show more"
                                aria-haspopup="true"
                                color="inherit"
                                onClick={handleLogout}
                            >
                                Logout
                            </Button>
                            <IconButton
                                color="inherit"
                                aria-label="open drawer"
                                edge="start"
                                onClick={handleDrawerToggle}
                                className={classes.menuButton}
                            >
                                <MenuIcon />
                            </IconButton>
                        </div>
                    }
                </Toolbar>
            </AppBar>
        </div>
    );
};

export default MainToolbar;

