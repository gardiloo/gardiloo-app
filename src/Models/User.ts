import Post from './Post';

export interface UserAsObject {
    id: number;
    email: string;
    name: string;
    posts: Post[];
}

export default class User {

    private readonly _id: number;
    private readonly _email: string;
    private readonly _name: string;
    private readonly _posts: Post[];

    constructor(id: number, email: string, name: string, posts: Post[]) {
        this._id = id;
        this._email = email;
        this._name = name;
        this._posts = posts;
    }

    get id(): number {
        return this._id;
    }

    get email(): string {
        return this._email;
    }

    get posts(): Post[] {
        return this._posts;
    }

    get name(): string {
        return this._name;
    }

    get asObject(): UserAsObject {
        return {
            id: this.id,
            email: this.email,
            name: this.name,
            posts: this.posts,
        }
    }

    static createFromJSONObject(userObject: UserAsObject): User {
        const { id, email, name, posts } = userObject;
        return new User(id, email, name, posts);
    }

    static createArrayFromJSONObject(usersObjectArray?: UserAsObject[]): User[] {
        return !!usersObjectArray ? usersObjectArray.map(userAsObject => User.createFromJSONObject(userAsObject)) : [];
    }

    static createJSONObjectFromArray(users: User[]): UserAsObject[] {
        return users.map(user => user.asObject);
    }
}
