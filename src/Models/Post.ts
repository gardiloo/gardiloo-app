import User from './User';

export interface PostAsObject {
    id:        string;
    price:     string;
    title:     string;
    content:   string;
    isbn:      string;
    image:     string;
    createdAt: string;
    author:    User;
}

export default class Post {

    private readonly _id: string;
    private readonly _price: string;
    private readonly _title: string;
    private readonly _content: string;
    private readonly _isbn: string;
    private readonly _image: string;
    private readonly _createdAt: string;
    private readonly _author: User;

    constructor( id: string, price: string, title: string, content: string,
                 isbn: string, author: User, image: string, createdAt: string ) {
        this._id = id;
        this._price = price;
        this._title = title;
        this._content = content;
        this._isbn = isbn;
        this._image = image;
        this._createdAt = createdAt;
        this._author = author;
    }

    get id(): string {
        return this._id;
    }

    get price(): string {
        return this._price;
    }
    get title(): string {
        return this._title;
    }

    get content(): string {
        return this._content;
    }

    get isbn(): string {
        return this._isbn;
    }

    get author(): User {
        return this._author;
    }

    get image(): string {
        return this._image;
    }

    get createdAt(): string {
        return this._createdAt;
    }

    get asObject(): PostAsObject {
        return {
            id: this.id,
            price: this.price,
            title: this.title,
            content: this.content,
            isbn: this.isbn,
            author: this.author,
            createdAt: this.createdAt,
            image: this.image
        }
    }

    static createFromJSONObject(postObject: PostAsObject): Post {
        const { id, price, isbn, content, title, author, image, createdAt } = postObject;
        return new Post(id, price, title, content, isbn, author, image, createdAt);
    }

    static createArrayFromJSONObject(postsObjectArray?: PostAsObject[]): Post[] {
        return !!postsObjectArray ? postsObjectArray.map( postAsObject => Post.createFromJSONObject(postAsObject)): [];
    }

    static createJSONObjectFromArray(posts: Post[]): PostAsObject[] {
        return posts.map( post => post.asObject);
    }
}
