const config = {
    endpoints: {
        gardiloo_api: process.env.NODE_ENV === "production" ? "https://fierce-taiga-77783.herokuapp.com/" : "http://localhost:4000/",
    },
    api_keys: {
        google_books_api_key: 'AIzaSyBtOLbcPEnY1gDaGOecwztadyccOZh4Qlc'
    }
};

export default config;
